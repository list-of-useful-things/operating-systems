# Linux
* [Killing Processes that Don’t Want to be Killed](https://blog.xenproject.org/2018/08/01/killing-processes-that-dont-want-to-be-killed/)
* [A Gentle Introduction to tmux](https://hackernoon.com/a-gentle-introduction-to-tmux-8d784c404340)
* [The TTY demystified](https://www.linusakesson.net/programming/tty/)
* [Seven Surprising Bash Variables](https://zwischenzugs.com/2019/05/11/seven-surprising-bash-variables/)
* [59 Linux Networking commands and scripts](https://haydenjames.io/linux-networking-commands-scripts/)

# Windows
### Command-line installers & package managers
* [Scoop](https://scoop.sh/)
* [Chocolatey](https://chocolatey.org/)

# Plan 9
* [Plan 9 — The Documents (Volume 2)](http://9p.io/sys/doc/)

